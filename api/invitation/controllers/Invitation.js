'use strict';
const parse = require('csv-parse');
const stringify = require('csv-stringify/lib/sync');
const util = require('util');
const stream = require('stream');
const pipeline = util.promisify(stream.pipeline);

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  updateByInviteCode: async ctx => {
    try {
      const { email, invite_code, guests } = ctx.request.body;
      if (!invite_code) {
        throw 'Missing invite code';
      }

      if (email) {
        const invitation = await strapi
          .query('invitation')
          .update({ invite_code }, { email });
        if (!invitation) {
          throw 'Invitation does not exist';
        }
      }

      if (guests) {
        for (let i = 0; i < guests.length; i++) {
          await updateGuest(guests[i], invite_code);
        }
      }

      ctx.body = {};
      ctx.status = 200;
    } catch (err) {
      ctx.body = err;
      ctx.status = 500;
    }
  },

  retrieveGuestsByInviteCode: async ctx => {
    try {
      const { invite_code } = ctx.request.body;

      if (!invite_code) {
        throw 'Missing invite code';
      }

      const invitation = await strapi
        .query('invitation')
        .findOne({ invite_code });

      if (!invitation) {
        throw 'Invitation does not exist';
      }

      await strapi.query('invitation').update({ invite_code }, { seen: true });

      ctx.body = invitation;
      ctx.status = 200;
    } catch (err) {
      ctx.body = err;
      console.log(err);
      ctx.status = 500;
    }
  },

  importWithCsv: async ctx => {
    try {
      const parser = parse(); //CSV parser

      //Pipe the incoming stream of data to CSV parser stream.
      //Streams are used in HTTP to send large files.
      //Wait for the incoming stream to finish and process through the pipeline.
      await pipeline(ctx.req, parser); //Waiting for the pipeline to finish like this before processing data could run out of memory with huge CSV.

      //Read from the finished parser stream
      let record;
      let counter = 0;
      while ((record = parser.read())) {
        await importRow(record);
        counter++;
      }

      ctx.body = `Imported ${counter} invitations.`;
      ctx.status = 200;
    } catch (err) {
      console.log(err);
      ctx.body = err;
      ctx.status = 500;
    }
  },

  exportCsv: async ctx => {
    try {
      const users = await strapi.query('guest').find();

      let rows = users.map(
        ({
          name,
          rsvp,
          diet,
          diet_other,
          allergies,
          invitation: { invite_code, seen, email }
        }) => {
          return [
            invite_code,
            seen ? 'Yes' : 'No',
            email,
            name,
            rsvp ? 'Yes' : rsvp === false ? 'No' : '',
            allergies,
            diet,
            diet_other
          ];
        }
      );

      rows.unshift([
        'Invite Code',
        'Seen',
        'Email',
        'Guest',
        'RSVP',
        'Allergies',
        'Diet',
        'Diet (Other)'
      ]); //Prepend to array

      ctx.attachment('guests.csv');
      ctx.type = 'text/csv';
      ctx.body = stringify(rows);
      ctx.status = 200;
    } catch (err) {
      console.log(err);
      ctx.body = err;
      ctx.status = 500;
    }
  }
};

const createInvitation = async inviteCode => {
  const invitation = await strapi
    .query('invitation')
    .create({ invite_code: inviteCode, seen: false });

  return invitation.id;
};

const createGuest = async (name, invitationId) => {
  await strapi.query('guest').create({
    name,
    invitation: invitationId
  });
};

const importRow = async row => {
  //First elem of row is the invite code
  const inviteCode = row[0];

  //The rest of the array is the guest names
  const guestNames = row.slice(1);

  const invitationId = await createInvitation(inviteCode);

  for (const guestName of guestNames) {
    if (guestName && guestName.length > 0) {
      await createGuest(guestName, invitationId);
    }
  }
};

const updateGuest = async (guest, invite_code) => {
  const { id, rsvp, allergies, diet, diet_other } = guest;

  if (!id) {
    throw 'Missing guest id';
  }

  if (!invite_code) {
    throw 'Missing invite code';
  }

  if (rsvp == null) {
    throw 'Missing rsvp';
  }

  if (allergies == null) {
    throw 'Missing allergies';
  }

  if (!diet) {
    throw 'Missing diet';
  }

  if (diet === 'Other' && !diet_other) {
    throw 'Diet is set to other but missing diet_other';
  }

  const result = await strapi.query('guest').findOne({ id });

  if (!result) {
    throw 'Guest does not exist';
  }

  if (result.invitation.invite_code !== invite_code) {
    throw 'Guest does not belong to this invitation';
  }

  await strapi
    .query('guest')
    .update({ id }, { rsvp, allergies, diet, diet_other });
};
